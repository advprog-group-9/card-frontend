import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
let playerId = Math.floor(Math.random() * 100000);

export default new Vuex.Store({
  state: {
    username: "",
    playerId: playerId,
    signedIn: false
  },
  mutations: {
    updateUsername(state, username) {
      state.username = username;
    },
    signIn(state) {
      state.signedIn = true;
    }
  },
  actions: {},
  modules: {}
});
